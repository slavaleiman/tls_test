#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gnutls/gnutls.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <stdbool.h>

#define CHECK(x) assert((x)>=0)
#define MAX_BUF 1024
#define MSG "GET / HTTP/1.1\r\nHost: %s\r\n\r\n"

static int tcp_connect(char* host, char* port)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd, s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    s = getaddrinfo(host, port, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) 
    {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                     rp->ai_protocol);
        if (sfd == -1)
            continue;
        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
            break;

        close(sfd);
    }
    if (rp == NULL) { // No address succeeded
        fprintf(stderr, "Could not connect\n");
        exit(EXIT_FAILURE);
    }
    freeaddrinfo(result);
    return sfd;
}

static void tcp_close(int sd)
{
        shutdown(sd, SHUT_RDWR);        /* no more receptions */
        close(sd);
}

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s host port output\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    int fd = open(argv[3], O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    int ret;
    gnutls_session_t session;
    gnutls_certificate_credentials_t xcred;

    CHECK(gnutls_global_init());
    CHECK(gnutls_certificate_allocate_credentials(&xcred));
    CHECK(gnutls_certificate_set_x509_system_trust(xcred));

    int sd = tcp_connect(argv[1], argv[2]);

    CHECK(gnutls_init(&session, GNUTLS_CLIENT));
    CHECK(gnutls_server_name_set(session, GNUTLS_NAME_DNS,
                                argv[1],
                                strlen(argv[1])));
    gnutls_session_set_verify_cert(session, argv[1], 0);

    CHECK(gnutls_set_default_priority(session));

    gnutls_transport_set_int(session, sd);
    gnutls_handshake_set_timeout(session,
        GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

    gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);

    do{
        ret = gnutls_handshake(session);
    }while (ret < 0 && gnutls_error_is_fatal(ret) == 0);

    if(ret < 0)
    {
        fprintf(stderr, "*** Handshake failed\n");
        gnutls_perror(ret);
        goto end;
    } else {
        printf("- Handshake was completed\n");
    }

    char get_request[256];
    sprintf(get_request, MSG, argv[1]);
    gnutls_record_send(session, get_request, strlen(get_request));

    char buffer[MAX_BUF + 1];

    do{
        memset(buffer, 0, sizeof(buffer));
        ret = gnutls_record_recv(session, buffer, MAX_BUF);

        if (ret == 0){
            printf("- Peer has closed the TLS connection\n");
            goto end;
        }else if (ret < 0 && gnutls_error_is_fatal(ret) == 0){
            fprintf(stderr, "*** Warning: %s\n",
                gnutls_strerror(ret));
        }else if (ret < 0){
            fprintf(stderr, "*** Error: %s\n",
                gnutls_strerror(ret));
            goto end;
        }

        if(ret > 0)
        {
            static bool is_ok = false;

            if(!is_ok)
            {
                char* substr = strstr(buffer, "200 OK");
                if(substr){
                    is_ok = true;
                    substr = strstr(buffer, "﻿<!DOCTYPE html>");
                    const int retval = write(fd, substr, ret);
                    if (retval == -1)
                        fprintf(stderr, "cannot write %d\n", fd);
                    fprintf(stderr, "%s", buffer);
                }
            }else{
                char* substr = strstr(buffer, "</html>");
                if(substr){
                    ret = substr - buffer + strlen("</html>");
                }
                const int retval = write(fd, buffer, ret);
                if (retval == -1)
                    fprintf(stderr, "cannot write %d\n", fd);
                fprintf(stderr, "%s", buffer);
                if(substr)
                    goto end;
            }        
        }
    }while(ret > 0);

end:
    close(fd);
    gnutls_bye(session, GNUTLS_SHUT_RDWR);
    tcp_close(sd);
    gnutls_deinit(session);
    
    gnutls_certificate_free_credentials(xcred);
    gnutls_global_deinit();
    return 0;
}
