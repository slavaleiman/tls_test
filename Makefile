
SRCS=\
client.c 

PROJ_NAME=client

CC=gcc

CFLAGS  = -g -Wall -std=c99 -O3 -D_GNU_SOURCE
# LDFLAGS = `pkg-config gnutls --cflags --libs`
LDFLAGS = -lgnutls

.PHONY: proj
all: proj
proj: $(PROJ_NAME)

$(PROJ_NAME): $(SRCS)
	$(CC) $(CFLAGS) $^ -o $@ $(LDFLAGS)

clean:
	rm -f *.o $(PROJ_NAME)
	find . -type f -name '*.o' -print0 | xargs -0 -r rm
